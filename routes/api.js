var express = require('express');
var router = express.Router();

//Models
var news = require('../models/news');

//Routes
news.methods(['get','put','post','delete']);
news.register(router,'/news');

//Return router
module.exports = router;