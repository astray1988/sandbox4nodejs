var restful = require('node-restful');
var mongoose = restful.mongoose;

//Schema

var newsSchema = new mongoose.Schema({
	id:String,
	url:String,
	city:String,
	score:String
});

//Return model
module.exports = restful.model('news',newsSchema);